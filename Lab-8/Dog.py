'''
Demo code to show basic concepts of a class
'''

class Dog:

    # Class Attribute
    # Same for all instances
    species = 'mammal'

    # Initializer / Instance Attributes
    # Different for different instances
    # Needs at least one argument apart from self
    def __init__(self, name, age):
        self.name = name
        self.age = age

    # instance method
    def description(self):
        return "{} is {} years old".format(self.name, self.age)

    # instance method
    def speak(self, sound):
        return "{} says {}".format(self.name, sound)

    # instance method
    def birthday(self):
        oldAge = self.age
        self.age=self.age+1
        return "{}'s old age was {}, and new age is {}".format(self.name, oldAge, self.age)

# Instantiate the Dog object
teddy = Dog("Teddy", 5)
# call our instance methods
print(teddy.description())
print(teddy.speak("Bhow, I am a young dog"))
# We can change the values of the instance through the behaviour
print(teddy.birthday())


# Instantiate another Dog object
# Notice the class attribute (species) is same for both objects
oreo = Dog("Oreo", 9)
print(oreo.description())
print(oreo.speak("Gruff, I am the leader of the pack"))
print(oreo.birthday())
print("Teddy and Oreo's species: ", teddy.species, oreo.species)
