class BankAccount: 
    """Bank Account protected by a pin number."""

    def __init__(self, pin): 
        """Initial account balance is 0 and pin is 'pin'."""
        self.__pin = pin
        self.balance = 0

    def deposit(self, pin, amount): 
        """Increment account balance by amount and return new balance."""
        if pin==self.__pin:
            self.balance+=amount
            return self.balance

    def withdraw(self, pin, amount): 
        """Decrement account balance by amount and return amount withdrawn."""
        if pin==self.__pin:
            if self.balance >= amount:
                self.balance -= amount
                return amount
            else:
                return "Balance not sufficient"

    def get_balance(self, pin): 
        """Return account balance."""
        if pin==self.__pin:
            return self.balance

    def change_pin(self, oldpin, newpin): 
        """Change pin from oldpin to newpin."""
        if self.__pin == oldpin:
            self.__pin = newpin

    def get_pin(self):
        return self.__pin


# bal = BankAccount("0000")
# print(bal.get_balance("0000"))
# bal.deposit("0000",89)
# b = bal.withdraw("0000", 2)
# print(b)

# # pin is private variable, should not be accessible from outside
# print(bal.pin)