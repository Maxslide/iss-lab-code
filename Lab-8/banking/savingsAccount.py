from bankAccount import BankAccount

class SavingsAccount(BankAccount):
	"""Savings Account with interest."""

	def interest(self):
		if self.balance <= 200:
			self.balance-=self.balance*0.05
		else:
			self.balance-=self.balance*0.1			


# sav = SavingsAccount(1111)
# sav.deposit(1111, 250)
# sav.interest()
# print(sav.get_balance(1111))